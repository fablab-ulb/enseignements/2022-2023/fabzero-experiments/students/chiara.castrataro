
Hello!

Bienvenue sur mon blog! Vous trouverez ici quelques informations sur moi et mon parcours. Bonne lecture!
# Qui suis je ?

Je m'appele Chiara. J'ai 22 ans et je suis en MA2 ingénieure électromecanique option Energy.
![](images/pp.jpg)


## Mon parcours 
En secondaire, je me suis rendue compte que j'aimais vraiment bien les maths et mon professeur m'avait conseillé de passer l'examen d'entrée d'ingénieurie. 
Je suis donc arrivée à l'école polytechnique de Bruxelles en ne sachant pas trop ce qui m'attendait. Durant mes études, je me suis rendue compte qu'en plus des maths, 
j'avais une vraie passion pour l' électromecanique et en particulier pour la géneration d'électricité. Maintenant que je suis à la fin de mon parcours, un tas d'opportunités s'offrent à moi et j'en suis très heureuse.

## Mes projets

Durant mes études, j'ai eu l'occasion de faire de nombreux projets tel que celui de ma première année qui fut de construire un séchoire à poivre solaire 
ou bien encore mon projet de BA2: la construction d'un robot de secourisme dont le but était de decendre une échelle et de deposer une trousse de secours. 
En MA2, j'ai eu un projet dont le but était de fournir une école en électricité à partir de panneaux solaire et de la chauffer grace à une chaudière aux pellets.



