# 2. Conception Assistée par Ordinateur (CAO)

The aim of this week is to become familiar with the different types of programs in order to make a CAD. 
## The different mechanisms
Two types of mechanisms are discussed in this section: The compliant mechanism and the bistable mechamism. 
### Compliant mechanism
[Compliant mechanism](https://en.m.wikipedia.org/wiki/Compliant_mechanism) is defined as a 
flexible mechanism that achieves force and motion transmission through elastic body deformation. It gains some or all of its motion from the relative flexibility of its members rather than from rigid-body joints alone.

### Bistable mechanism 
[Bistable mechanism](https://www.researchgate.net/publication/259291073_Compliant_bistable_mechanisms#:~:text=Bistable%20mechanisms%20are%20mechanisms%20that,and%20despite%20small%20external%20disturbances.) is defined as a mechanism that has two stable equilibrium positions within their range of motion. Its advantages include the ability to stay in two positions without power input and despite small external disturbances. These various mechanisms can be 3D printed.

 However, they must first be designed in a dedicated program.

## 3D softwares

There are many 3D softwares and they fall into two broad categories, depending on if they deal with raster (pixel) images or vector images.

1. **Raster software**  that processes images by pixels where every image is an infinite set of colour points. Examples of raster software are *Paint*, *Gimp*, *Blender*, ...

2.  **Vectorial software** that treats images with boundaries that are set by the vectorial geometry such as straight lines, polygons, and arcs. The vectorial images can be enlarged infinitely and
are lighter than raster images.
Examples of vectorial software are *Solidworks*, *OpenScad*, *Freecad*, *Inscape*, ...

One of the advantages of vector software is that you can change a factor of the piece and the latter will be modified accordingly.

## OpenScad
This software works by describing the geometry of a part by writing code. [Cheatsheet](https://openscad.org/cheatsheet/) contains all the useful functions you need to know to start a project. Some functions allow you to write code (operators, constants, variables, ...) and some functions allow you to describe geometrical forms.

## Operators
| code |  Description  |    
|-----|-----------------|
| `union()`| union of 2 forms | 
| `difference() ` | a difference between the two forms (a subtraction, thus extracting a part of the)| 
| `hull()` | link two forms in a certain way without seeing the difference between the forms|

## Transformation
| code |  Description  |    
|-----|-----------------|
| `rotate()`| rotation by giving the different arguments in x,y,z | 
|` translate()` | translation according to the three axes of freedom| 

## Geometrical forms
| 2D|  3D | arguments |
|-----|-----|----------|
|`circle()`| sphere() | *radius* or d = *diameter*|
|`square()`  | cube() | *size*, *center [width,(depth,) height], center*
|`polygon() `| | *[points]* ou *[points], [paths]*|
|          | cylinder()| *height, radius or diameter, center*|


Note that it is important to **parameterize** the functions in order to save time on a modification or a retouch.

# Licences
There are different types of licenses, we will focus more on the [Creative Commons](https://creativecommons.org/about/cclicenses/) licenses because they give the possibility to others to be inspired and to use our work. In this course, the purpose is to share about some innovation so it is important to leave our project in creative license. There exist different options:

| Options| Meaning |
|-----|------------|
| *BY*| *Credit must be given to the creator*|
|*SA*| *Adaptations must be shared under the same terms*|
|*NC*| *Only noncommercial uses of the work are permitted*|
|*ND*| *No derivatives or adaptations of the work are permitted*|


# Model of FlexLinks using 3D CAD software

First, i annonce the liscence that i will used.
```
// File : flexi1.scad

// Author : Chiara Castrataro

//Date : 25 février 2023

// license : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
```
## Parametrisation
First of all, I defined the parameters:
```
diameter = 5; // diamater of the main form
rd = diameter/3; // radius of the holes
x_cube= 10; // length of the branch
y_cube=1; // height of the branch
z_cube=1; // thickness of the branch
$fa = 1; // $fa is the minimum angle for a fragment. Even a huge circle does not have more fragments than 360 divided by this number. The default value is 12 (i.e. 30 fragments for a full circle). The minimum allowed value is 0.01. Attempting to set a lower value causes a warning.

$fs = 0.8; // $fs is the minimum size of a fragment. The default value is 2 so very small circles have a smaller number of fragments than specified using $fa. The minimum allowed value is 0.01. Attempting to set a lower value causes a warning.
```

## Creation of the main form inspierd by FlexLinks
I create a form based on a cylinder where, thanks to the function scale, I double de size of the thickness cylinder. The second step is to create the two holes present on this form. For that purpose, I use the function `difference()` that allows me to substracte two cylinder that I re-shape using `scale()`. I can choose the position of the two hole thanks to the function `translate()`
```
difference() {
scale([2,1,1])cylinder(d=diameter);
   
    translate([diameter/2, 0,-1])cylinder(10,rd,rd);
    translate([-diameter/2, 0,-1])
    cylinder(10,rd,rd);
}
```
This form is illustrated in the Figure bellow:
![](images/flex11.jpg)




Then I will create the second part of my Flexlink which has the same form that the one described above. To place this second part I will use the function `translate()`.
```
difference() {
scale([2,1,1])cylinder(d=diameter);
   
    translate([diameter/2, 0,-1])cylinder(10,rd,rd);
    translate([-diameter/2, 0,-1])
    cylinder(10,rd,rd);
}
translate([(diameter*2)+x_cube, 0,0])
difference() {
scale([2,1,1]) // double the x cordinate
    cylinder(d=diameter);
  translate([diameter/2, 0,-1])cylinder(10,rd,rd);
    translate([-diameter/2, 0,-1])
    cylinder(10,rd,rd);
}
```

Finally, I want to create a bransh in order to join the two piece parts. Here, you can see how I implement this branch:
```
translate([diameter, -y_cube/2, 0])
cube([x_cube,y_cube,z_cube]);
```
The code above gives me:

![](images/cube.JPG)

In order to join the part 1, the part 2 and the branch, I use the function `union()`.
```
union(){
difference() {
scale([2,1,1])cylinder(d=diameter);
   
    translate([diameter/2, 0,-1])cylinder(10,rd,rd);
    translate([-diameter/2, 0,-1])
    cylinder(10,rd,rd);
}

translate([diameter, -y_cube/2, 0])
cube([x_cube,y_cube,z_cube]);

translate([(diameter*2)+x_cube, 0,0])
difference() {
scale([2,1,1]) // double the x cordinate
    cylinder(d=diameter);
  translate([diameter/2, 0,-1])cylinder(10,rd,rd);
    translate([-diameter/2, 0,-1])
    cylinder(10,rd,rd);
}
}
```
The code above gives me my final FlexLink :

![](images/flex2.JPG)