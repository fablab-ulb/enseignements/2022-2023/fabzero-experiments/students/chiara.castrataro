# 4. Laser Cutter

This week is dedicated to learning tools from the Fab lab. I have selected the laser-cutters.

## Security 
Before coming to the formation, we were asked to be aware of several rules of good conduct - [Laser cutter](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut/-/blob/main/Laser-cutters.md)

### The steps to follow: before, during and after the cutting

First, the steps to follow **before** starting the cutting:
- Have knowledge about the **material** that will be cut.
- Know where to find a **CO_2 extinguisher**.
- Know where to find the **emergency stop button**. 
- Always turn on the **smoke extractor**.
- Always activate **compressed air** (except for Epilog, it is automatic).

The steps to follow **during** cutting:
- Wear proper **safety glasses**.
- Do not watch the impact of the **laser beam** for too long.
- Stay **close to the machine** until the end of the cutting.

The steps to follow **after** cutting:
- Do not open the machine while there is **smoke** inside
-  Remove the **residue** from the cut (if necessary, use a vacuum cleaner)

### Cutting mateials
It is important to be careful with the materials used as not all are compatible with the machine and some can even emit toxic fumes.
There are several recommended materials such as:
- **Acrylic** (PMMA/Plexiglas)
- **Raw wood**
- **Plywood/multiplex wood**
- **Textiles**
- **Paper**, **cardboard**

On the other hands, there are unrecommended materails such that:
- **MDF**: thick smoke and very harmful in the long term.
- **ABS** , **Polystyrene** (PS): melts easily, harmful smoke.
- **PE**, **PET**: melts easily.
- and more - all [unrecommed materials](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut/-/blob/main/Laser-cutters.md)

It is even more crucial to be aware of the **forbidden** materials:
- **PVC**: acidic and very harmful smoke
- **Copper**: totally reflects the laser
- **Teflon** (PTFE): acidic and very harmful smoke
- and more - all [forbidden materials](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut/-/blob/main/Laser-cutters.md)

## During the formation - Group assisgment

During the formation, different kinds of laser cutters have been presented: *Epilog Fusion Pro 32*, *Lasersaur* and the  *Full Spectrum Muse*.

We then formed groups to get to know how to use one of the three machines. My group had to learn how to use the **Full Spectrum Muse**.

### Full Spectrum Muse characteristics 

This laser cutter has a cutting area of 50 x 30 cm and can work with materials up to 6 cm thick. It has a 40 W CO2 laser and a red pointer for accuracy. This is major advantages because it allows to facilitate the positioning of the sketch with respect to the cutting material. The software is Retina Engrave, which is user-friendly and allows you to adjust settings and import designs. [Here](https://www.youtube.com/playlist?list=PL_1I1UNQ4oGa0w55C772Y1mC6F4f3ZcG6), you can find a tutorial to become familiar with this software.

![](images/muse.jpg)

### Full Spectrum Muse operation

The first things to do, it is to connect the *Full Spectrum Muse* to Wi-fi either via Ethernet cable or a dedicated Wi-Fi network. The second step is to enter the IP address, given on the machine's screen, on the search browser. That allows to access Retina Engrave. Files compatible with this program include SVG (vectorial) as well as BMP, JPEG, PNG and TIFF (rasters). 

Note that a laser cutter is able to perform different processes such as **Cutting**, **Vectorial engraving**, and **Raster engraving** :
- **Cutting** : this is done following a vector path. You need to import a vector file (for example in .svg format).
For thicker materials, it is often necessary to make several passes to cut through.
- **Vectorial engraving**: Same technique as for cutting but with less power. The lines are therefore engraved at a shallow depth.
- **Raster engraving**: it allows to engrave a drawing on the surface of the material.

When performing laser cutting, two important parameters to consider are **power** and **speed**. These two parameters are mutually dependent and together they determine the quality and precision of the cut. Indeed, higher laser power increases the chances of cutting materials in one pass, but also increases the risk of burning. Conversely, too low a power may not fully cut the material. Lower speeds may cause melting or unwanted marks. However, high speeds may result in imprecise cuts.

Making a material test is a common practice to determine the effects of cutting power and speed on a material. The latter for variation in cutting parameters and provides samples for each combination. By observing the cut quality for each parameter combination, optimal parameters for the material can be determine. An advantage of this method is that it quickly optimizes cutting parameters and prevents costly errors. Moreover, the material test can be kept as areference, making it easy to reproduce the optimal cut for that specific material.

With my group, we chose a material test on this [site](https://fslaser.com/material-test/). The chosen test is the *vector test* and you can see the result below:

![](images/vectortest.jpg)