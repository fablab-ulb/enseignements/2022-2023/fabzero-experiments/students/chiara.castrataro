# 1. Gestion de projet et documentation

The purpose of this first module is to become familiar with different tools that will allow me to document the FabZero course. 

## Documentation 

It is important to document the whole process of a project. This means to explain how the final result was achieved and any problems encountered in the process. It is important to have the most honest documentation possible and therefore to explain both the failures and the successes that happens during the process. This will help anyone reading my documentation to learn from my mistakes and understand how I arrived at the final result of my project. Moreover, the documentation will allow me to remember the steps I took and record everything I achieved in the process. 

Here is the list of tools that will help me to document the course:

- GitLab
- Git Bash 
- Git 
- Markdown 

All these tools are explained in detail in the sections below.

## GitLab

The main **GitLab** features are:
- respository 
- version control (git)
- CI/CD
- issues

**GitLab** allows me to modify my website, while conserving previous versions of my documentaion through the Gitlab.com GUI. In order to do this:

1. Log in to [GitLab](https://gitlab.com/)
2. Find your project sapce in the [FabZero-Experiment respository](https://gitlab.com/fablab-ulb/enseignements/2022-2023/)
3. Open your project and modify the markdown file (.md) in order to modify your website.


The next step is to create a copy of the project on my computer. Working on a "project copy" has different advantages like :
- It allows to do all the modifications that you want on a local version on your computer before you "commit" to the distant server.
- You can work in team at the same time on a shared project.
- You can work on your project at any time and everywhere.
- It is faster and you do not need to log on to [GitLab](https://gitlab.com/).


To this end, the first step is to install GitBash.

## Git Bash

**Git Bash** is an application that enables you to install the bash environment on a Windows operating system. After the installation, I had to become familiar with the command terminal. Here is a list of the commands I used:


| Commande |  Description  
|-----|-----------------  
| `pwd`| Print working directory 
| `cd`| Change directory  
| `cp file.txt`  | Copy files and folders 
| `mkdir`  |  Create a directory 
| `ls`  |  List files and folders 



## Git

**Git** is a tool for tracking changes made to a set of files over time, while preserving previous versions. The most commonly used Git commands are:

| Commande |  Description  
|-----|-----------------  
| `git pull` |Permits me to take the latest changes from the server and merge them with the version on my PC. 
| `git push ` | Send the changes on the local directory to the server. 
| `git add -A`  | Add new changes
|  `git commit -m “Message” `   | Saves changes to the local directory. It creates a snapshot of the changes and saves them in the Git version history.
| `git status`| Checks the current status of the local directory against that on the server. It displays modified, added or deleted files.

## SSH keys

### Why SSH keys?

The purpose of the **SSH** keys is to have a secure connection between my computer and the server where my website is located (GitLab). There exist 2 different keys:

- The **public** key for encryption.
- The **private** key for decryption which is confidential.

It is possible to communicate with gitlab via different types of SSH keys, the one I used is **ED25519** . According to the book [Practical Cryptography With Go](https://leanpub.com/gocrypto/read#leanpub-auto-chapter-5-digital-signatures), these keys are more secured and performant than RSA keys. 

### Creation of the secure connection between GitLab and my PC

I followed this [tutorial](https://docs.gitlab.com/ee/user/ssh.html) which led me to achieve the following steps: 

1. **Generate SSH keys** : Open a terminal and go into ` C:/Users/username/.ssh/directory ` . You can see here how I generate my SSH key pair: ![](images/Capture.PNG)


2. **Link the SSH keys to my GitLab account** :I first copy my public key.
![](images/copy.PNG)


 I then connect to GitLab and go to *User Setting* on the *SSH keys* tab and add my public key. You can check that everything went well via the command `ssh -T git@gitlab.com`.


###  Clonage of the Git repository from the sever to my PC

To this end, I go to my GitLab repository and I copy the **link**  " Clone with SSH". 


I open then a terminal and I go in the directory where I want to clone my repository. I can now clone my repository by using the following line code `git clone` **link** 

I can now work directly on the copy of the project on my computer.

## Markdown

**Markdown** is a language that allows me to do the layout of the site. The advantage of Markdown is that it is easy to read and write. Here are some elements of Markdown syntax that I found on [Markdown Cheat Sheet](https://www.markdownguide.org/cheat-sheet/):

| Commande |  Description  
|-----|-----------------  
| `# Title`| # Title
| `##`| ## Subtitle
| `**bold**`  | **bold**
| `*italics*`  |  *italics*
| ``   |  `code` 
|  `[name of the site](url)`  |  insert a website link
|  `![image title](images/image.jpg)`  |  insert an image 





## Image compression 
Compressing images allows for fast display and reduces the amount of storage space required. Let's start by looking at some of the existing formats:

- JPEG: This format has the advantage of compressing large images efficiently but at the cost of losing some precision.
- PNG: This format allows to compress images without any loss (contrary to JPEG).

For this course, a slight loss of image quality is acceptable, so I will mostly use JPEG.
In order to compress your image you can use a tool like [gimp](https://www.gimp.org/downloads/). A tutotrial to use it is available in this [link](https://www.gimp.org/tutorials/GIMP_Quickies/#changing-the-size-dimensions-of-an-image-scale).


## Project management
In order to maintain the rhythm and not forget what was done during each module, I will try to write my documentation as soon as possible after the course. This will allow me to stay up to date and not have too much work at the end.












