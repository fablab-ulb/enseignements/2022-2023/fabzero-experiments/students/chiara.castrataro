# 5. Dynamique de groupe et projet final

## 5.1 Project analysis and design

The problem tree aims to analyze a specific issue, represented by a tree structure with the main **problem** as the trunk, its **causes** as the roots, and its **consequences** as the branches. In the second step, this problem tree is transformed into an **objective** tree, where the roots become **activities** to develop to reach the objective steps and the branches represent the resulting **outcomes**. 

Now that we understand how a problem tree works, let's create one with [Karl Preux](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/karl.preux/). On the basis of the 17 United Nations goals, we have developped an issue about the access to sustainable and reliable energy for all. 

![](images/tree_1.jpg)

The second step is to transform this problem tree into an objective tree:

![](images/tree_2.jpg)

The conclusions derived from this objective tree are as follows:
Sustainable and reliable energy access can reduce energy independence and mitigate conflicts and price fluctuations. In the Mediterranean region, countries could used their abundant resources like solar, hydro, and wind power in order to reduce fossil fuels consumption. However, due to the intermittency of renewable energy, coupling with storage or with another enrgy sources such as nuclear energy.

## 5.2 Group formation and brainstorming on project problems

Before coming to the class, we had to think of an object to bring to the course. This object had to represent a theme or a societal issue that was important to us. In my case, I brought a picture from my journey to Cape Verde. This memory was related to the ocean which is omnipresent there and which is something that is very important to me. 

During the course, we had to form groups based on the objects we had brought. I formed a group with: [Emma](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/emma.dubois/fabzero-modules/module05/), [Martin](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/jean-francois.loumeau/), [Suzanne](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/suzanne.lipski/), and [Alicia](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/Alicia.gimza/).

The common point between our objects was the ocean and the journey. Then we discussed the different issues that could be linked to these themes:
- pollution marine transport
- oil spill
- marine waste such as microplastics
- marine tourism
- overfishing

## 5.3 Group dynamics
This unit focuses on learning of tools to comminicate better within a group. Skilss in structuring meetings, defining and assigning roles, utilizing decision-making techniques, promoting equal participation, and providing constructive feedback will be developed.

Three tools that I have learned are described below:

1. The art of making decisions:

There are three possibilities when making a group decision: the first is to make the decision all together because it is a decision in which all members of the group must participate. The second possibility is that one of the members has additional knowledge about the decision to be made and therefore relies on him to make the decision. The third possibility is to postpone the decision until later because the group does not have enough information to make the decision directly.

2. Move hands up and down:
 
- When you are really in favour of an idea => Raise your hands up.
- When you are not against but not for either  => Put your hands out straight in front of you. 
- When you are really against the idea => Put your hands down.

Ask the team what they think about an idea and all team members should place their hands. The advantage of this technique is that a decision can be made quickly and it is an efficient way to know what everyone is thinking.

3. Meeting plan:

A meeting plan is essential for the smooth running of a team project by keeping things structured and helping to organize the work. A typical plan includes a weather forecast to gauge the mindset of the participants, an outline of the tasks to be discussed, the meeting itself, and the distribution of tasks.

