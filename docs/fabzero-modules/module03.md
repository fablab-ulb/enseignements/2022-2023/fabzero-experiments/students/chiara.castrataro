# 3. Impression 3D
The purpose of this module are:
- Identify the advantages and limitations of 3D printing.
- Apply design methods and production processes using 3D printing.

This third module aims to teach us how to use slicing software and a 3D printer. 

## 3D printing technologies 

One of the advantages of 3D printing is its ability to manufacture very complex parts using interesting materials that can reduce the weight and cost of the printed pieces. Note that there exist different [types of 3D printing technology](https://www.protolabs.com/resources/blog/types-of-3d-printing/). Some of them are described below:
 
- Stereolithography (**SLA**): This 3D printing technology uses a liquid resin that is cured with a laser or UV light to create a solid object.
- **S**elective **L**aser **S**intering (**SLS**): This type of 3D printing uses a laser to melt and fuse together powdered materials, such as plastic, metal, or ceramic, to create a solid object. 
- **F**used **D**eposition **M**odeling (**FDM**): This is the most popular type of 3D printing technology, which works by melting a thermoplastic filament and depositing it layer by layer to create a 3D object. This is the technology used in the FabLab and therefore the one I learned to use during this module. The latter is described in the section below.

## **F**used **D**eposition **M**odeling (**FDM**)

As stated above, the **FDM** works by melting a thermoplastic filament and depositing it layer by layer to create a 3D object. This filaments must be able to melt and resolidify quickly and the materials able to do this are limited (you can read about the materials that can be used for this 3D printing technology [here](https://help.prusa3d.com/category/material-guide_220)). Note that, depending on the material, the printing parameters must be adapted, in particular, the temperature. The working principle of FDM printers  is the following: A heated nozzle moves in the X, Y, and Z axes to deposit the melted filament onto a build plate, where it solidifies and bonds with the layer beneath it. More information on this technology can be found [here](https://xometry.eu/en/fused-deposition-modeling-fdm-3d-printing-technology-overview/).


Now I have a good understanding of the technology behind the type of 3D printing used at the FabLab. I will apply my knowledge by printing the piece modeled during [module 2]().
The first step is to install a slicer.

## Slicer

A [slicer](https://en.wikipedia.org/wiki/Slicer_(3D_printing)) is a software application that transforms computer-generated 3D models created using CAD software into instructions that 3D printers can comprehend and execute. These programs, in particular, convert STL files (or their equivalents) into G-code files that include essential printing parameters, such as print precision and the use of supports. The slicer I chose is the [PrusaSlicer](https://www.prusa3d.com/page/prusaslicer_424/#_ga=2.237927488.1554251034.1678271016-1872888702.1678271016)  because it is compatible with the Prusa 3D printers which are the ones used in FabLab.

## Printing my Flexlink piece

I downloaded the STL file of my piece created during module 2 and opened it with PrusaSlicer :

![](images/C1.jpg)

Following then the [instructions](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/class/-/blob/master/vade-mecum/3D_print.md), I have defined the parameters but note that there are default parameters such that the "*hauteur de couche*". 

![](images/C2.jpg)

Regarding the filling, for a decorative structure, a 15% filling is sufficient, while more robust structures require a 25% to 30% filling. As for the pattern, the Honeycomb ("*nid d'abeille*in the picture") design is suitable for dense structures, while the Grid ("*grille* in the picture") pattern is appropriate for simpler structures. As illustrated in the image below, for my piece, I want to have a strong structure this is why I choose the honeycomb pattern and a 30% filling.

![](images/C3.jpg)

It is important to note that to minimise the amount of material used, only use supports and borders when necessary. For my piece a support is not necessary. The materials used can be also set, in my case it is the PLA. In order to reduce the printing time, the scale of my piece is defined. Finally, thanks to the *hoverhang test* , it is possible to predict from which angle the strcuture becomes imperfect (60° in my case).

The last step is to export the file in the form of g-code and put on a SD card and then launched the printing.

![](images/IMG_6650.jpg)
